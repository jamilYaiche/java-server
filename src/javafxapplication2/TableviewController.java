/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication2;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.scene.control.TextField;
import javafx.stage.Stage;
import server.Rq;

/**
 * FXML Controller class
 *
 * @author lenovo
 */
public class TableviewController implements Initializable {
	
	
	 @FXML
	 private TextField i1;
	
	 @FXML
	 private TextField i2;
	 
	 @FXML
	 private TextField i3;
	 
	 @FXML
	 private TextField i4;
	 
	 @FXML
	 private TextField i5;
	 
	 @FXML
	 private TextField i6;
	 
	
	 
	 Rq rq;
	 
	 
	    
	
	private static FXMLDocumentController dController;
	
 public void changeScreenButtonPushede(ActionEvent event) throws IOException
  {
	 FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("FXMLDocument.fxml"));
	  Parent fxmldocument = (Parent) fxmlLoader.load();
	  
	  dController = fxmlLoader.getController();
      
      Scene tableViewScene = new Scene(fxmldocument);
  Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
  window.setScene(tableViewScene);
  window.show();
  
 }
 

 public void changeInfo(ActionEvent event) throws IOException
 {
	 FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("FXMLDocument.fxml"));
	  Parent fxmldocument = (Parent) fxmlLoader.load();
	  
	  dController = fxmlLoader.getController();
     
     Scene tableViewScene = new Scene(fxmldocument);
     Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
 
 		dController.ChangeInfo(i1,i2,i3,i3,i5,i6);
 		
 		window.setScene(tableViewScene);
 		window.show();
 
}


 
 
 
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    	rq = new Rq();
    	
    	i1.setText(rq.getInfo(1));
    	i2.setText(rq.getInfo(2));
    	i3.setText(rq.getInfo(3));
    	i4.setText(rq.getInfo(4));
    	i5.setText(rq.getInfo(5));
    	i6.setText(rq.getInfo(6));
    	
    }    
    
}