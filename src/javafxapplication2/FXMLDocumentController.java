
package javafxapplication2;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.stage.Stage;
import server.Guichet;
import server.Rq;
import server.Server;
import server.Service;


public class FXMLDocumentController implements Initializable {
	
	Service [] service1 = new Service[5];
	Service [] service2 = new Service[5];
	Service [] service3 = new Service[5];
	
	private Service S1,S2,S3;

	Guichet guichet1,guichet2,guichet3;
    
	public static Server server;
	
	public static GuichetController gcontroller;
	public static TableviewController tcontroller;
	public static Rq rq;
	
    @FXML
    private Label label,i1,i2,i3,i4,i5,i6;  
  
    @FXML
    private CheckBox s1,s2,s3,s4,s5,s6,s7,s8,s9;
      
    @FXML
    private ToggleButton g1,g2,g3,btn;
  
   public void Initialisation(ActionEvent event) throws Exception {
	  
	   
	   if(s1.isSelected()) 
		   service1[1]=S1;
	   if(s2.isSelected()) 
		   service1[2]=S2;
	   if(s3.isSelected()) 
		   service1[3]=S3;
	   
	   if(s4.isSelected()) 
		   service2[1]=S1;
	   if(s5.isSelected()) 
		   service2[2]=S2;
	   if(s6.isSelected()) 
		   service3[3]=S3;
	   
	   if(s7.isSelected()) 
		   service3[1]=S1;
	   if(s8.isSelected()) 
		   service3[2]=S2;
	   if(s9.isSelected()) 
		   service3[3]=S3;
	   
	   if(g1.isSelected()) {
		   g1.setStyle("-fx-background-color: #CD5C5C");
		   guichet1 = new Guichet(1,service1);
	   }
	   else
		   g1.setStyle("-fx-background-color: #49a7c4");
	 
	   if(g2.isSelected()) {
		   g2.setStyle("-fx-background-color: #CD5C5C");
		   guichet2 = new Guichet(2,service2);
	   }
	   else
		   g2.setStyle("-fx-background-color: #49a7c4");
	   
	   if(g3.isSelected()) {
			   g3.setStyle("-fx-background-color: #CD5C5C");
			   guichet3 = new Guichet(3,service3);
		   }
		   else
			   g3.setStyle("-fx-background-color: #49a7c4");
	   
   }
    
  public void changeScreenButtonPushed(ActionEvent event) throws IOException
  {
	  Stage stage = null;
  
  
  FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Tableview.fxml")); //chargement de l'interface de type fxml
  Parent tableView= (Parent) fxmlLoader.load();
  tcontroller = fxmlLoader.getController();
  stage = new Stage();
  stage.setScene(new Scene(tableView));  
  stage.show();
  
  }
  
  // @SuppressWarnings("null")
   @SuppressWarnings("null")
public void AfficherGuichet(ActionEvent event) throws IOException
  {
	   Stage home = null;
	   
	   if(btn.isSelected()) {
	
		   btn.setStyle("-fx-background-color: #CD5C5C");
	   		
	   
	   FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Guichet.fxml"));
	   Parent root1 = (Parent) fxmlLoader.load();
	   gcontroller = fxmlLoader.getController();
	   home = new Stage();
	   home.setScene(new Scene(root1));  
	   home.show();

	   
	  
	   server = new Server(guichet1,guichet2,guichet3,rq,gcontroller);
	

	   //server.Init();
	   server.start();

	
	   System.out.println("Server is running");
 

	   }else 
		   if(!btn.isSelected()) {
			   
			   btn.setStyle("-fx-background-color:  #afd982");
			   home.close();
			   
		   }
			   
  }
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	
    	 rq = new Rq();
    	 

    	Service S1 = rq.getService(1);
  		Service S2 = rq.getService(2);
  		Service S3 = rq.getService(3);
    	
    	s1.setText(S1.getNom());
    	s2.setText(S2.getNom());
    	s3.setText(S3.getNom());
    	
    	s4.setText(S1.getNom());
    	s5.setText(S2.getNom());
    	s6.setText(S3.getNom());
    	
    	s7.setText(S1.getNom());
    	s8.setText(S2.getNom());
    	s9.setText(S3.getNom());
    	
    	i1.setText(rq.getInfo(1));
    	i2.setText(rq.getInfo(2));
    	i3.setText(rq.getInfo(3));
    	i4.setText(rq.getInfo(4));
    	i5.setText(rq.getInfo(5));
    	i6.setText(rq.getInfo(6));
    	
    	
    	        
    }    
    
    public void ChangeInfo(TextField label1,TextField label2,TextField label3,TextField label4,TextField label5,TextField label6) {
    	
    	i1.setText(label1.getText().toString());
    	i2.setText(label2.getText().toString());
    	i3.setText(label3.getText().toString());
    	i4.setText(label4.getText().toString());
    	i5.setText(label5.getText().toString());
    	i6.setText(label6.getText().toString());
    
    	
    	rq.setInfo(label1.getText().toString(),label2.getText().toString(),label3.getText().toString(),label4.getText().toString(),label5.getText().toString(),label6.getText().toString());
    	
    }
    
   
    
   
    
}