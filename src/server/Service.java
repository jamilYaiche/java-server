package server;

public class Service {

	private int id;
	private String Nom;
	private int Temps_estime;
	
	public Service(String Nom) {
		
		this.setNom(Nom);
	}

	public Service(String nom, int temps_estime) {
		super();
		Nom = nom;
		Temps_estime = temps_estime;
	}
	
	public Service(int id, String nom, int temps_estime) {
		super();
		this.id = id;
		Nom = nom;
		Temps_estime = temps_estime;
	}

	public String getNom() {
		return Nom;
	}

	public void setNom(String nom) {
		Nom = nom;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getTemps_estime() {
		return Temps_estime;
	}

	public void setTemps_estime(int temps_estime) {
		Temps_estime = temps_estime;
	}
}