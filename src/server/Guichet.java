package server;

public class Guichet {
	
	private int Num;
	private Service[] service ;
	private int nbu = 0;
	
	public int getNbu() {
		return nbu;
	}

	public void IntNbu() {
		this.nbu++;
	}

	public Guichet(int Num ,Service[] service) {
		
		this.Num=Num;
		this.service = service;
	}

	public int getNum() {
		return Num;
	}

	public void setNum(int num) {
		Num = num;
	}

	public Service getService(int n) {
		return service[n];
	}

	public void setService(Service[] service) {
		this.service = service;
	}
	

}