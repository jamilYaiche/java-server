package server;


import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;


public class Ticket implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String num;
	private int service;
	private boolean retard;
	private LocalDate date;
	private LocalTime time;
	private long code;
	private int Temps_estime;
	
	public Ticket(String num, int service, LocalDate date, LocalTime time,int temps_estime) {
		super();
		this.num = num;
		this.service = service;
		this.date = date;
		this.time = time;
		Temps_estime = temps_estime;
		
	}
	
	public Ticket(String num, int service, LocalDate date, LocalTime time,int temps_estime, boolean b) {
		super();
		this.num = num;
		this.service = service;
		this.date = date;
		this.time = time;
		Temps_estime = temps_estime;
		this.retard=b;
	}
	

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public int getService() {
		return service;
	}

	public void setService(int service) {
		this.service = service;
	}

	public boolean isRetard() {
		return retard;
	}

	public void setRetard(boolean retard) {
		this.retard = retard;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
		
	}

	public Long getCode() {
		return code;
	}

	public void setCode(Long code) {
		this.code = code;
	}



	public LocalTime getTime() {
		return time;
	}

	public void setTime(LocalTime time) {
		this.time = time;	
	}
	
	public String toString()  {
		
		return code+"/"+num+"/"+service+"/"+date+"/"+time+"/"+Temps_estime;
	
	}
	

	public int getTemps_estime() {
		return Temps_estime;
	}

	public void setTemps_estime(int temps_estime) {
		Temps_estime = temps_estime;
	}
	
	
	
	

}
