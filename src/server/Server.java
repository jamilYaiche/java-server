package server;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javafxapplication2.GuichetController;




		public class Server extends Thread {
			
			private ServerSocket ss;
			Socket s;
			private PrintWriter pw;
			
			public static int nbr = 0 ; // nbr ticke global 
			public static int nbremove = 0; 
			
			private Map<Integer,Ticket> m =new HashMap<Integer,Ticket>(); //file d'attente 
			
			private Guichet guichet1;// les guichets 
			private Guichet guichet2;
			private Guichet guichet3;
		
			
			public static int n1 = 0 ;// nbr de ticket par service 
			public static int n2 = 0 ;
			public static int n3 = 0 ;
			
			public static int r1 = 0 ;
			public static int r2 = 0 ;
			public static int r3 = 0 ;
			
			public static int t1 = 0 ;// temps estim� par service 
			public static int t2 = 0 ;
			public static int t3 = 0 ;
			
			public static LocalDate d;
			public static LocalTime tempe;
			
			
		
			String ch = "";
			
			private Rq rq ; //requette de connexion avec bd 
			
			
			
			private GuichetController controller; // 
			
			
			public Server(Guichet guichet1,Guichet guichet2,Guichet guichet3,Rq rq,GuichetController controller) {
				
				this.guichet1= guichet1;
				this.guichet2= guichet2;
				this.guichet3= guichet3;
				this.rq = rq;
				this.controller=controller;
	
			}
			
			public void Init() {
				this.rq.Init_DB();
			}
	
		public void run() {
		
			try {
				ss = new ServerSocket(8080);

			
            while (true) {

            	s =ss.accept();
				new Conversation(s).start();
				
				}
			
				} catch (IOException e) {
					
					e.printStackTrace();
				}}// delaration du serveur socket 

		class Conversation extends Thread{
			private Socket socket;
			
			
			public Conversation(Socket socket)
			{
				super();
				this.socket=socket;
			}
			
			public void run() {
				
				int rep; // reponse du socket 
				
				try {
					  
					InputStream is = socket.getInputStream();
					InputStreamReader isr=new InputStreamReader(is);
					BufferedReader br = new BufferedReader(isr);
					
					OutputStream os = socket.getOutputStream();
				    pw = new PrintWriter(os , true);
				    
				
				    
					rep = Integer.parseInt(br.readLine());
					
					
					if (rep==1)
					{
						
						n1++;
						t1+=10;
						System.out.println(t1);
					
						Enfiler(n1,(int)rep,t1); // enfilement 
						
						this.socket.close();
						
						affiche();
						System.out.println("****************************");
						
					}
					else
					if (rep==2)
					{
					
						n2++;
						t2+=15;
						
						Enfiler(n2,(int)rep,t2);
						
						this.socket.close();	
						
						affiche();
						System.out.println("****************************");
						
						
					}
					
					if (rep==3)
					{
						
						n3++;
						t3+=5;
						
						Enfiler(n3,(int)rep,t3);
					
						this.socket.close();	
						
						affiche();
						System.out.println("****************************");
						
						
					}
					else 
						if (rep==4)
						{
							pw.println("Guichet 1");
								
							controller.setT1(Defiler(guichet1)); //defilement + affichage 
							
							
							this.socket.close();	
							
							System.out.println("****************************");
					
						}
						else 
							if (rep==5)
							{
								pw.println("Guichet 2");
									
								controller.setT2(Defiler(guichet2));
									
								
								this.socket.close();	
								
								System.out.println("****************************");
						
							}
							else 
								if (rep==6)
								{
									pw.println("Guichet 3");
										
									controller.setT3(Defiler(guichet3));
										
									
									this.socket.close();	
							
									System.out.println("****************************");
							
								}else if (rep==7)
								{
									
									n1++;
									t1+=10;
									long r = Enfiler2(n1,1,t1);
									pw.println(r);

									this.socket.close();	
									
									affiche();
									System.out.println("****************************");
									
								}
								else
								if (rep==8)
								{
								
									n2++;
									t2+=15;
									pw.println(2546);
									pw.println(Enfiler2(n2,2,t2));//enfilement
									
									this.socket.close();	
									
									affiche();
									System.out.println("****************************");
									
									
								}
								
								if (rep==9)
								{
									
									n3++;
									t3+=5;
									pw.println(2546);
									pw.println(Enfiler2(n3,3,t3));
									
									
									this.socket.close();	
									
									affiche();
									System.out.println("****************************");
									
									
								}
								else if (rep > 12)  // recup�ration du ticket apartir du code de r�servation 
								{
									
									rq = new Rq();
									String s1 = rq.getRes(rep);
									if (s1 == null) {
										pw.println("Ce numero n'existe pas");
									}else {
									pw.println(s1);}
									this.socket.close();
								}
										
				} //fin try 
				catch (IOException e) {
					
					e.printStackTrace();
				}
			
			}//fin run 
			
		}//fin conversation 

		public Ticket Enfiler(int n,int num,int Te) {
	
			d = LocalDate.now();
			tempe = LocalTime.now();
			
			nbr++;
			
			
			rq=new Rq();
			Ticket t = new Ticket(" N� T"+num+"-"+n,num,d,tempe,Te);
			
			long req = rq.addRes(t); // ajouter a la base + generation du code de reservation unique 
			
			ch=" N° T"+num+"-"+n+"/"+num+"/"+req;
			
			t.setCode(req);
			
		
			pw.println(t.toString()); // envoi du ticket 
			pw.flush();
			pw.close();
			
			
			m.put(nbr,t);
			
			return t;
		
		}
		
		public long Enfiler2(int n,int num,int Te) {
			

			nbr++;
			long req = 0;
			
			rq=new Rq();
			Ticket t = new Ticket(" N� R"+num+"-"+n,num,d,tempe,Te,true);

			req = rq.addRes(t);
			
			ch=" N° R"+num+"-"+n+"/"+num+"/"+req;
			
			t.setCode(req);
			
			m.put(nbr,t);
		
			return req;
		}
		
		public void Reservation(int n,int num) {
			
			
			
			
		}
		
		public String Defiler(Guichet guichet)
		{
			boolean b = false ;
			String CH = null ;
		
			Set<Entry<Integer,Ticket>> setHm = m.entrySet();
		      Iterator<Entry<Integer,Ticket>> it = setHm.iterator();
		 
		      while(it.hasNext()&&!b){
			         Entry<Integer,Ticket> e = it.next();
			
		
		
		Ticket ch=e.getValue();

		
		if(guichet.getService(ch.getService())!=null) { // kol guichet ilawej all awel ticket mel les services mte3ou 
			
			b=true;
			
			System.out.println("Guichet : "+guichet.getNum()+ch.getNum()+" Service :"+guichet.getService(ch.getService()).getNom() +"code : "+ch.getCode());
			
			CH = ch.getNum();
			
			rq.Delete(ch.getCode(),guichet.getNum()); // effacer de la base 	
			
			m.remove(e.getKey()); // efface de la file 
			
		}
		}
		      if(b==false)
		    		System.out.println("n'existe pas");
				
		      return CH;
		}


		public void affiche() // affichage de la file 
		{
			Set<Entry<Integer,Ticket>> setHm = m.entrySet();
		      Iterator<Entry<Integer,Ticket>> it = setHm.iterator();
		 
		      while(it.hasNext()){
			         Entry<Integer,Ticket> e = it.next();
			         if(e.getValue()==null)
			        	 
			        	 System.out.println(e.getKey()+ " : " +e.getValue());
			         else 
			        	  System.out.println(e.getKey()+ " : " +e.getValue().toString());
		      }
		}
		
		
		

	
}